# Prezentace - testování a py.test

# Zobrazení

* Stáhnout [repozitář](https://gitlab.com/ondra.voves/slides_pytest/repository/archive.zip?ref=master)
* Rozbalit 
* Otevřít v prohlížeči `slides.html`

# Build

* Nainstalovat pandoc
* Spustit
    ```bash
    $ pandoc -s -i -t revealjs slides.md -o slides.html --slide-level=2 --variable theme=blood
    ```
    
* Otevřít v prohlížeči `slides.html`
