import sqlite3

import pytest


@pytest.fixture
def db(request):
    # Setup
    conn = sqlite3.connect(':memory:')

    yield conn.cursor()

    # Cleanup
    conn.close()


def test_db(db):
    assert isinstance(db, sqlite3.Cursor)
