import pytest


@pytest.fixture(params=[1, 2, 3, 4])
def fix_data(request):
    return request.param


def division(a, b):
    return a / float(b)


def test_division_param_fix(fix_data):
    assert division(fix_data, 1) == fix_data
