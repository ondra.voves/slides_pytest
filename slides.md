% Testování a py.test
% Ondra Voves

# Proč testovat?

## BUGY

* Platí pravidlo: **Čím dříve se na bug příjde tím menší škodu napáchá => tím méně stojí času a pěněz**

## WORKFLOW

* Snadněji se **refaktoruje**
* Snadněji se **přidává funkcionalita**
* Programátor **nemusí** zkoumat a znát všechná zákoutí projektu => **rychlejší** zaučení nových členů + **zkrácení** vývojového cyklu
* Možnost pokročilejšího workflow (**CI**, **CD**)

## Continual Integration

* Automaticka kontrola projektu jako celku
    * Features se merguji do mastera kde se automaticky otestuje integrita změn

## Continual Deploy

* Automatické nasazování (produkce, test)
    * Merge do větve (**production**, **test**)
        * Nasazeni na stroje (**docker**, **fabric**)

# Testování v Pythonu

## Výhody testování pythonem

* Snadnější **čtení**, **psaní**, **spouštění**
* Pamět a CPU **nejsou** bottleneck 
* Nasazování **není** problém
* **Nemusí** se testovat jen python kód (C, Webovky, Monitoring, atp...)

# py.test

## Co je py.test?

* Open source testovací framework ![pytest_stars] ![pytest_contributors]
* Aktivně se vyvíjí od roku 2007 a neustále se zlepšuje
* Není součást standartní knihovny, ale v komunitě je známý
* Instalace:
 
    ```bash
    $ pip install pytest
    $ pip3 install pytest
    ```

[pytest_stars]: https://img.shields.io/github/stars/pytest-dev/pytest.svg
[pytest_contributors]: https://img.shields.io/github/contributors/pytest-dev/pytest.svg

## Proč používát py.test

* Kompatibilní s **unittest** and **nosetest** (Aby se usnadníl přechod)
* Není tak komplikováný jak by se na první pohled mohlo zdát
* Pomáhá psát lepší testy => lepší kód => lepší software
* Dobrá dokumentace, Komunita
* Pokročilé funkce, pluginovatelnost (+150)
* Používá například **Fedora** a **Mozilla**

# První test

## Ukázka

```python
def division(a, b):
    return a / float(b)

def test_division():
    assert division(1, 1) == 1
```

## Spouštění

```bash
$ py.test test_division.py
```

## OK
![](img/divison_ok.png)

## Fail
![](img/divison_error.png)

## Spouštění testů (1/2)

* V aktuálním adresáři rekurzivně projde složký **test**, **tests** a hledá soubory ve formatu **test_\*.py** 
    ```bash
    $ py.test
    ```

* Všechny testy v souboru **test_module.py**
    ```bash
    $ py.test test_module.py 
    ```

## Spouštšní testů (2/2)

* Test **test_foo** v souboru **test_module.py**
    ```bash
    $ py.test test_module.py::test_foo
    ```

* Všechny testy s keyword **foo** a bez **bar** v souboru **test_module.py** 
    ```bash
    $ py.test test_module.py -k "foo not bar"
    ```

# Skip

## Skipování testu

* Jsou případy kdy cheme aby testy procházeli přesto že nějaký test padá

## Simple

```python
import pytest

@pytest.mark.skipif()
def test_skipped():
    assert 1
```

## Conditional

```python
import pytest
import sys

@pytest.mark.skipif(sys.version_info[0] == 3,
                    reason="Only Python 2")
def test_skipped():
    assert 1
```

![](img/skipif.png)

# Parametrizace testu

* Způsob oddělení logiky testu od testovacích dat.
* Snadné rozšiřovaní testů.
* Čistější testy.

## Ukázka

```python
import pytest

def division(a, b):
    return a / float(b)
    
@pytest.mark.parametrize("a", [1, 2, 3, 4])
def test_division_param(a):
    assert division(a, 1) == a
```

![](img/param.png)

# Značkování

## O co jde?

* Každy test se da označit libovolným textem/značkou
* Skupina testů
* Abstraktnější organizace testu (pomale testy, readonly, db testy)

## Ukázka (1/2)

```python
import pytest
import time


@pytest.mark.slowtest
def test_slow():
    time.sleep(1)
    assert True


def test_fast():
    assert True
```

## Ukázka (2/2)

![](img/mark.png)

![](img/mark_slowtest.png)

# Fixtures

## Co jsou fixtures

* Testovací kontext (**Test setup**, **Test cleanup**)
* Testovací data (**uživatel s právama**, **prohlížec s načtenou stránkou**, **data v DB**)
* Dependency injection

## První fixture

```python
import pytest

@pytest.fixture
def structure():
    class MyStruct(object):
        text = "text"
        num = 10
    return MyStruct()

def test_fix(structure):
    assert isinstance(structure.text, str)
    assert isinstance(structure.num, int)
```

## Fixture scope

* Scope určuje lifetime fixtury.
    ```python
    import pytest
    @pytest.fixture(scope="function")
    def foo(): pass
    ```

* **function** - Fixture se vytvoří pri spuštení testu a po skončení se zníčí
* **module** - Fixture se vytvoří pro jeden python module
* **session** - Fixture se vytvoří pro celý běh testů

## Setup/Cleanup

* Pomocí **yield**
    ```python
    import sqlite3
    import pytest
    
    @pytest.fixture
    def db(request):
        # Setup
        conn = sqlite3.connect(':memory:')
    
        yield conn.cursor()
    
        # Cleanup
        conn.close()
    
    
    def test_db(db):
        assert isinstance(db, sqlite3.Cursor)
    ```

## Parametrizace fixture (1/2)

```python
import pytest

@pytest.fixture(params=[1, 2, 3, 4])
def fix_data(request):
	return request.param


def division(a, b):
	return a / float(b)


def test_division_param_fix(fix_data):
	assert division(fix_data, 1) == fix_data
```

## Parametrizace fixture (2/2)

![](img/param_fix.png)

# Struktůra testu

## Adresářová struktůra
    project_root/
        |_ src/
        |_ tests/ # root testu
            |_ test_unit/ # Unit testy
            |   |_ test_product/
            |        |_ test_product_model.py
            |        |_ ...
            |        |_ conftest.py
            |
            |_ test_integration # Integracni testy
            |   |_ test_product_creation.py
            |   |_ ...
            |
            |_ conftest.py
            
## conftest.py (1/2)

* Umožnuje rozšiřovat py.test 
    * Přidavat argumenty (--url-addr)
    * Hooks atp...
* Sdílet fixtury => fixtura která je definovana v conftest.py je automatický přístupná v testech na stejné nebo zanořené ůrovní.

## conftest.py (2/2)

* py.test automaticky natahuje všechny **conftest.py** v cestě od rootu testu k danému modulu kde se test nachází
    * test_product_model.py
        * project_project_root/tests/conftest.py
        * project_root/tests/test_unit/test_product/conftest.py

# Reference

## Pluginy

* [pytest-xdist][pytest_xdist] - Paralelizace testů
* [pytest-cov][pytest_cov] - Coverage => Meření pokrytí testy
* [pytest-splinter][pytest_splinter] - Hight-level API na testování webovek ("nadstavba" nad seleniem, phantomjs)
* [pytest-bdd][pytest_bdd] - BDD testovani s podporou Gherkin language
* Django, sqlalchemy, flask, ...

[pytest_xdist]: https://pypi.python.org/pypi/pytest-xdist
[pytest_cov]: http://pytest-cov.readthedocs.io/en/latest/readme.html
[pytest_splinter]: http://pytest-splinter.readthedocs.io/en/latest/
[pytest_bdd]: https://pypi.python.org/pypi/pytest-bdd

## Další zdroje

* [Py.test](https://www.pytest.org)
* [Introduction to pytest](http://slides.com/jeancruypenynck/introduction-to-pytest/embed)
* [Unit test fetish](http://250bpm.com/blog:40)
* [Gitlab-ci enviroments](https://docs.gitlab.com/ce/ci/environments.html)
